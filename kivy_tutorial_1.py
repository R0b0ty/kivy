import kivy
from kivy.app import App
from kivy.uix.button import Label
kivy.require('1.9.0')


class HelloKivy(App):
    def build(self):
        return Label(text="Hello Kivy")


kivy_obj = HelloKivy()
kivy_obj.run()
